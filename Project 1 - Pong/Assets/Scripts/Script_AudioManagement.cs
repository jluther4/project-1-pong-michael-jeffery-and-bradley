﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_AudioManagement : MonoBehaviour
{
  public static Script_AudioManagement instance = null;
  public AudioSource backgroundMusic;
  public AudioSource soundSFX;

  void Awake ()
  {
    if (instance == null)
    {
      instance = this;
    }
    else if (instance != null)
    {
      Destroy(this.gameObject);
    }
    DontDestroyOnLoad(this.gameObject);
  }

  public void BackgroundMusic(AudioClip soundToPlay)
  {
    backgroundMusic.clip = soundToPlay;
    backgroundMusic.Play();
  }

  public void SoundSFX (AudioClip soundToPlay)
  {
    soundSFX.clip = soundToPlay;
    soundSFX.Play();
  }
}
