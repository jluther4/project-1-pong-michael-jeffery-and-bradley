﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author: Jeffery Luther

public class Script_Ball : MonoBehaviour
{
  public float minX = -15f;
  public float maxX = 15f;
  public float speed = 5f;
  public AudioClip bounceSFX;
  public Vector3 startingPosition;
  private Rigidbody rb;
  private GameObject scoreBoard;

	// Use this for initialization
	void Start ()
  {
    rb = GetComponent<Rigidbody>();
    scoreBoard = GameObject.Find("Scoreboard_Prefab");
    StartCoroutine(GameDelay());
	}

  IEnumerator GameDelay()
  {
    transform.position = startingPosition;
    rb.velocity = Vector3.zero;
    rb.constraints = RigidbodyConstraints.FreezeAll;

    yield return new WaitForSeconds(2f);

    float startX = Random.Range(-3, 3);
    float startY = Random.Range(-3, 3);
    rb.constraints = RigidbodyConstraints.FreezePositionZ;
    rb.velocity = new Vector3(speed * startX, speed * startY, 0f);
  }

  void Update ()
  {
    if (transform.position.x > maxX)
    {
      if(gameObject.CompareTag("Ball"))
      {
        scoreBoard.GetComponent<Script_GameManagement>().PlayerOneScores();
      }
      StartCoroutine(GameDelay());
    }

    if (transform.position.x < minX)
    {
      if (gameObject.CompareTag("Ball"))
      {
        scoreBoard.GetComponent<Script_GameManagement>().PlayerTwoScores();
      }
      StartCoroutine(GameDelay());
    }
  }

  void OnCollisionEnter (Collision col)
  {
    if (col.gameObject.CompareTag("Left Bumper") || col.gameObject.CompareTag("Right Bumper")
      || col.gameObject.CompareTag("Boundary") || col.gameObject.CompareTag("Ball") || col.gameObject.CompareTag("Decoy") || col.gameObject.CompareTag("Breakthrough"));
    {
      Script_AudioManagement.instance.SoundSFX(bounceSFX);
    }
  }
}
