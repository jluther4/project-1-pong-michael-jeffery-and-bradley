﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_GameManagement : MonoBehaviour
{

  public Text playerOneScoreText;
  public Text playerTwoScoreText;
  public Text gameOverText;
  public Transform gameOverPanel;
  public Transform gameOverMenuPanel;
  public Transform pauseScreen;

  public AudioClip backgroundMusic;

  public int scoreToWin;
  public int playerOneScore;
  public int playerTwoScore;

	// Use this for initialization
	void Start ()
  {
    playerOneScore = 0;
    playerTwoScore = 0;
    Script_AudioManagement.instance.BackgroundMusic(backgroundMusic);
	}
	
	// Update is called once per frame
	void Update ()
  {
		if (Input.GetKeyDown(KeyCode.P))
    {
      Pause();
    }
	}

  public void Pause()
  {
    if (pauseScreen.gameObject.activeInHierarchy == false)
    {
      pauseScreen.gameObject.SetActive(true);
      Time.timeScale = 0;
      AudioListener.volume = 0;
    }
    else
    {
      pauseScreen.gameObject.SetActive(false);
      Time.timeScale = 1;
      AudioListener.volume = 1;
    }
  }

  public void PlayerOneScores()
  {
    playerOneScore += 1;
    playerOneScoreText.text = playerOneScore.ToString();

    if (playerOneScore == scoreToWin)
    {
      gameOverPanel.gameObject.SetActive(true);
      gameOverMenuPanel.gameObject.SetActive(true);
      gameOverText.text = "Player 1 Wins!";
      Time.timeScale = 0;
      AudioListener.volume = 0;
    }
  }

  public void PlayerTwoScores()
  {
    playerTwoScore += 1;
    playerTwoScoreText.text = playerTwoScore.ToString();

    if (playerTwoScore == scoreToWin)
    {
      gameOverPanel.gameObject.SetActive(true);
      gameOverMenuPanel.gameObject.SetActive(true);
      gameOverText.text = "Player 2 Wins!";
      Time.timeScale = 0;
      AudioListener.volume = 0;
    }
  }
}
