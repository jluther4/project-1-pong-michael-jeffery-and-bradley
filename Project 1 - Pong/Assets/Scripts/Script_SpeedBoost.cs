﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_SpeedBoost : MonoBehaviour
{
  // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
  {
		if(Input.GetKeyDown(KeyCode.LeftShift))
    {
      GetComponent<Script_Bumper>().leftBumperSpeed = 20;
    }

    if(Input.GetKeyUp(KeyCode.LeftShift))
    {
      GetComponent<Script_Bumper>().leftBumperSpeed = 10;
    }

    if (Input.GetKeyDown(KeyCode.RightShift))
    {
      GetComponent<Script_Bumper>().rightBumperSpeed = 20;
    }

    if (Input.GetKeyUp(KeyCode.RightShift))
    {
      GetComponent<Script_Bumper>().rightBumperSpeed = 10;
    }
  }
}
