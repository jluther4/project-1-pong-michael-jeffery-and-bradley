﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_BreakthroughBlocks : MonoBehaviour
{
  public int blockLives;
  
  void OnCollisionEnter (Collision col)
  {
    if(col.gameObject.CompareTag("Ball"))
    {
      blockLives--;
      if(blockLives == 0)
      {
        Destroy(this.gameObject);
      }
    }
  }
}
