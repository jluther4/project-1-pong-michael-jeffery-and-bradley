﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_SceneManagement : MonoBehaviour
{
  public void _MainMenu()
  {
    SceneManager.LoadScene("Scene_MainMenu");
  }

  public void _LevelSelect()
  {
    SceneManager.LoadScene("Scene_LevelSelection");
  }

  public void _LevelOne()
  {
    SceneManager.LoadScene("Scene_LevelOne");
    Time.timeScale = 1;
    AudioListener.volume = 1;
  }

  public void _LevelTwo()
  {
    SceneManager.LoadScene("Scene_LevelTwo");
    Time.timeScale = 1;
    AudioListener.volume = 1;
  }

  public void _LevelThree()
  {
    SceneManager.LoadScene("Scene_LevelThree");
    Time.timeScale = 1;
    AudioListener.volume = 1;
  }

  public void _Credits()
  {
    SceneManager.LoadScene("Scene_Credits");
  }

  public void _Replay()
  {
    string sceneName = SceneManager.GetActiveScene().name;
    SceneManager.LoadScene(sceneName);
    Time.timeScale = 1;
    AudioListener.volume = 1;
  }

  public void _ExitGame()
  {
    Application.Quit();
  }
}
